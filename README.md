﻿# Spike Report

## Packaging an executable

### Introduction

We’ve now got a nearly complete, testable game.

We need to be able to distribute it to people who are not on our development team – so it’s time to package the game up into an Executable!

Our game currently targets PC, but we could use a similar process to learn how to deploy for Android, iOS, or a Console.

### Goals

1. Knowledge: What is “Cooking” in the Unreal Engine?
1. Skill: How to package our game into an executable

### Personnel

* Primary - Max Finn
* Secondary - N/A

### Technologies, Tools, and Resources used

* [Packaging and Cooking Games](https://docs.unrealengine.com/latest/INT/Engine/Deployment/)
* [Reducing APK Package Size](https://docs.unrealengine.com/latest/INT/Platforms/Android/ReducingAPKSize/)

### Tasks Undertaken

1. To reduce the size of our APK, navigate to Edit > Project Settings > Packaging and set the following properties:
    - `Create compressed cooked packages`: true
    - `Exclude editor content when cooking`: true
    - `Build configuration`: Shipping
1. Navigate to File > Package Project > Windows > Windows (64-bit) to cook and package the project, creating the executable file (and a number of other files) in a folder location of our choice.

### What we found out

In UE4, Cooking is the process of converting the engine's internal file storage formats (such as .png for textures) to match that of the various target platforms.

When previously using UE4, packaging a project would normally result in a 300+ megabyte folder, even for simple, singular scene games. Using the first two settings listed above, this size dropped to around 180MB, and after switching the Build configuration to Shipping, it dropped further to roughly 140MB. This still feels far too large for a game as minimal as the one I have produced, but this is likely representative of the amount of behind the scenes work UE4 does (although I suspect there would still be other size reductions that could be made).

### Open Issues/Risks

1. Packaging the project to run outside of the editor appears to have broken the 1920 x 1080 resolution option (which in my case is also the resolution of my screen). Menu elements are no longer centered, appear slightly blurry, and their hitboxes become misaligned (presumably they stay in the correct spots whilst the visual elements incorrectly scale).
    * This is presumably happening because with the addition of the window frame, the screen does have enough room to display everything (i.e. this issue would not occur on something like a QHD monitor).

### Recommendations

To fix the resolution issue that has been introduced, it may be helpful to investigate how both full screen and windowed full screen would be implemented in UE4, and adjust the Options menu to reflect this.